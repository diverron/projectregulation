### Documentation

Dependencies:
 
	sudo apt-get update
	sudo apt-get install python-pip
	sudo apt-get install python-dev
	sudo pip install paramiko
	sudo pip install mercurial
	sudo pip install pycrypto
	sudo pip install ecdsa


### Steps to execute Script as Background:

	•	cd new_regulations/
	•	chmod 400 experimentNew.pem (Or whatever .pem you use)
	•	edit server_info.txt (if you add or remove any node)
	•	python fetch_regulations.py (This will distribute work load among all available nodes accordingly including master node)
	•	cd ~ 
	•	cd federal_regulations
	•	screen -R work (initiate separate background session)
	•	python server.py > logs.txt
	•	control + a + d (To exit screen)
	•	cat logs.txt (When script execution is complete this file is updated with Failed document ids)
	•	cd data
	•	ls -l | wc – l (to check how many files have been downloaded so far)
	•	cd ~ (When you are sure script have done its work)
	•	cd new_regulations/
	•	python download_regulations.py (it will create a folder records which will have all documents downloaded on all nodes)
	•	cd records
	•	ls -l | wc – l (to check how many files have been collected so far from all nodes)

### Configuring  Child Node:

Dependencies:
 
	•	sudo apt-get update
	•	sudo apt-get install python-pip
	•	sudo apt-get install python-dev

Steps to execute Script as Background:

	•	cd ~ 
	•	cd federal_regulations
	•	screen -R work (initiate separate background session)
	•	python server.py > logs.txt
	•	control + a + d (To exit screen)
	•	cat logs.txt (When script execution is complete this file is updated with Failed document ids)
	•	cd data
	•	ls -l | wc – l (to check how many files have been downloaded so far)


### Troubleshoot

Make sure you have these packages installed and if you are using ubuntu; run,

	sudo apt-get --reinstall install python-pyasn1 python-pyasn1-modules
	apt-get install libffi-dev libssl-dev
	sudo pip install --upgrade ndg-httpsclient

### Information
	
	Server_info.txt 
	This file contains all ec2 instances host address whichare provided. Its possible to add more remote servers you can simply add them to this file, just make it sure it's private key it's same as 'experiment.pem'

	api_keys.txt
	Considering the rate limit of api call per hour which is 1000. We need more keys definitely. These are ten keys each dedicated to each server, if you intend add new remote server, you will need to add more keys here respectively. This also means that if you have executed the script, it will check 10 * 1000, document urls which need to be downloaded in an hour, then you have to wait for an hour to execute the script back and download new documents. This have to be repeated because we are going to download 5M documents which means 5000000/10000 = 500 Hours, rest you can do the math. In the current version we are using regulation.gov API which doesnt have limit restrictions.

	fetch_regulations.py
	This is our main file which does the major work with 5000 download urls which have about 1000 files each. It distribute the work considering the amount of remote servers it have. Rest of work which is about execution it commands the servers to do that. Anywhere you execute fetch_regulations.py, it will act as central server. But download_regulations.py is independent. It will copy all data from all servers to their local hard drive/storage, simply it download it on our pc.

	1. do_server_work 
		This functions creates a ssh remote connection to any server. It takes username, password, hostaddress as arguments, moreover it takes file paths which need to be automatically uploaded on host. It also takes commands which can be executed remotely and returns that command output. So basically all work we do manually by using SSH and SFTP it does it all.

	2. distribute_work 
	As the name suggest it starts calculating date string starting from 1-1-1994 till today, which we will pass as arguments to federal_regulations api as publication date. So it makes nearly 8200~ urls which have documents published on each date. After formulating these api urls we calculate how many nodes we have to distribute this load equally. Usually if we have 11 nodes we allocate 8200/11 urls to each nodes, and each node will be responsible to download documents from set of urls assigned to it, these urls are written in temporary file urls.txt. Now this function make use of 'do_server_work' and distribute urls.txt plus existing_document.txt file in case we are executing the script again.	


	server.py
	You don't have to execute it because, it is written to be uploaded on remote servers and do it's work there. Which means it will receive it's chunk of download urls from which it will fetch documents ids and then it will start downloading files in separate threads. Once download is complete it will write the file in data folder remotely created on server.

	1. make_records
		It reads all Api urls allocated to current node. This url sends json based document which have documents pdf urls to be downloaded so its sole responsibility is to collect all these pdf urls from api urls and start downloading them in separate thread.

	2. download_file
		It just download streaming pdf url file, and return errors if its unable to do that

	download_regulations.py
	Once server.py has done it's work. It will write files online on remote servers respectively so this file will be responsible to collect data files from all servers mentioned in server_info.txt and merge them into single local folder named as 'records'. 

	urls.txt
	This is insignificant file, just contains download urls chunks for each server. If you are executing the script first time MAKE SURE YOU DELETE THIS FILE. 

